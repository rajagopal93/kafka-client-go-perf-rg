module kafka-client-go-perf-rg

go 1.13

require (
	bitbucket.org/swigy/kafka-client-go v0.0.0-20200325101152-c586470f29f0
	github.com/google/uuid v1.1.1
)
